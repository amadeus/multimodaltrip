# Multi Modal Trip Recommendation

The aim of the project is to demonstrate what has been done in Next Trip Recommendation project, where the goal is to recommend destination to CEM Travellers based on their [historical trips](https://rndwww.nce.amadeus.net/git/projects/MUL/repos/multi_modal_trip_recommendation/browse/Model_M1) at first. A second model is also proposed to recommend destinations to any traveller that [uploads a picture of city](https://rndwww.nce.amadeus.net/git/projects/MUL/repos/multi_modal_trip_recommendation/browse/Model_M2), this recommendation is based on the keywords that characterize the uploaded image.

## Description

This repository is an API of the 2 recommendation models that can be used in order to make prediction of recommended destination of users based either on their profiles or on the image.

# Software dependencies

The following Python libraries are required for this project:

* [Numpy](https://www.numpy.org), version 1.16.2
* [Pickle](https://docs.python.org/2/library/pickle.html), version 4
* [Sklearn](https://scikit-learn.org/stable/), version 0.20.3
* [Tensorflow](https://www.tensorflow.org), version 1.4.0
* [Flask](https://palletsprojects.com/p/flask/) , version 1.0.2

To install these libraries, execute one of the following commands at the root of the project:

```shell
> pip install requirements.txt
```

# Directories structure

This project is organised as follows:

|    Directory    |                   Content                                             |
|-----------------|-----------------------------------------------------------------------|
|Model_M1         |   M1 code api + pyhon standalone code + pretrained tensorfllow        |
|Model_M2         |   M2 code api + python standalone code + pretrained cities embeddings |


# How to run the APIs?

### M1:

Go to Model_M1/src/ directory and execute the following commands:

```shell
> export FLASK_APP=M1_Api.py
> python -m flask run --host=0.0.0.0
```

Now, you are running Flask service in your localhost, the service is probably using (if not already used) the port 5000. You need then to launch [postman](https://www.getpostman.com) in order to make queries to the API M1. A query to the API M1 is defined as follows:
{"dpt_day": integer between 0 and 6 that corresponds to the day of the week, "nip": integer (max is 9) corresponding to the number of passenger in the reservation, "user_id": integers the id of the cem user, "top_k": integer that corresponds to how many recommended city you want to get}

Below, you can see an example on how to use postman for M1 API:

![Postman Usage on M1 API](Readme_Material/Model_M1_Example.PNG?raw=true "Postman Usage on M1 API")]


### M2:

Go to Model_M1/src/ directory and execute the following commands:

```shell
> export FLASK_APP=M2_Api.py
> python -m flask run --host=0.0.0.0
```

Now, you are running Flask service in your localhost, the service is probably using (if not already used) the port 5000. You need then to launch [postman](https://www.getpostman.com) in order to make queries to the API M1. A query to the API M1 is defined as follows:
{"image_path": image you want to use in order to get recommendations, "top_k": integer that corresponds to how many recommended city you want to get}

Below, you can see an example on how to use postman for M2 API:

![Postman Usage on M2 API](Readme_Material/Model_M2_Example.PNG?raw=true "Postman Usage on M2 API")]


