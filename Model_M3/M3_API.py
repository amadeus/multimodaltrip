from flask import Flask, url_for, request, jsonify
import json
import pickle
import sys
import os
import tensorflow as tf
import numpy as np
import pandas as pd
import collections
import argparse
import random
import requests
import time

sys.path.append('../Model_M2/src/')
sys.path.append('../Model_M2/utils/')
sys.path.append('../Model_M1/src/')
sys.path.append('../Model_M1/utils/')
import Model_M2
import Model_M1
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

## Model 1 preminilary instantiation
with open('../Model_M1/data/fin_user_item_interaction.pkl', 'rb') as f:
    user_item_interaction =  pickle.load(f)

print('*************************** Loading cities names & characteristics: ****************************************')

cities_index = np.load('../Model_M1/data/cities_name_index.npy')

item_charac = np.load('../Model_M1/data/norm_item_matrix.npy')

print('*************************** Loading user demographics: ****************************************')

user_charac = np.load('../Model_M1/data/norm_user_matrix.npy')

## Model 2

print('*************************** Loading cities Embeddings: ****************************************')
with open('../Model_M2/data/fastext_cities.pkl', 'rb') as f:
    dict_fastext_cities_key_words = pickle.load(f)

print('*************************** Loading Word Vectors: ****************************************')
with open('../Model_M2/data/dict_idf.pkl', 'rb') as f:
    dict_idf = pickle.load(f)
with open('../Model_M2/data/fastext_dictionary.pkl', 'rb') as f:
    dict_fasttext = pickle.load(f)

print('*************************** Instantiate Image Model: ****************************************')
model_m2 = Model_M2.Model_M2('../Model_M2/Google_Credentials/Cred.json', dict_fastext_cities_key_words, dict_idf, dict_fasttext)


def spotpix_image(country, city):
    
    # defining a params dict for the parameters to be sent to the API 
    PARAMS = {'country':country, 'city':city, 'format': "Custom 1"} 

    # sending get request and saving the response as response object 
    r = requests.get(url = "https://test-spotpix.1a-lab.net/v2/media/files/by-city",
                     params = PARAMS, headers={'x-api-key': '1JlltDlLB37dT227Kp8hj10lPOmDwaAegITvlm0i'}) 

    # extracting data in json format 
    image_url = r.json()['data']['attachmentUri']
    
    return image_url


#******************** Model_M1 *************************#
def model_m1_proba(dpt_day=3, nip=2, stay_duration=2, user_id=5):

    print('*************************** Instantiate DKFM Model : ****************************************')
    pwd = os.getcwd()
    os.chdir('../Model_M1/src/')
    model_m1 = Model_M1.Model_M1(user_item_interaction, cities_index, item_charac, user_charac, model='../models/DKFM_10.ckpt')
    
    print('*************************** Compute Model input: ****************************************') 
    X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_ = model_m1.compute_input(dpt_day, nip, stay_duration, user_id)
    
    print('*************************** Compute the top-k most similar cities: ****************************************')
    top_k_cities_m1 = model_m1.compute_recommendation(X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_, k=119, user_id=user_id)
    
    os.chdir(pwd)
    
    return top_k_cities_m1

#******************** Model_M2 *************************#
def model_m2_proba(image_path):

    print('*************************** Compute the image embedding: ****************************************')
    fastext_vector = model_m2.compute_image_fastext(image_path)

    print('*************************** Compute the top-k most similar cities: ****************************************')
    top_k_cities_m2 = model_m2.compute_top_k_similarity(fastext_vector, 119)
    
    return top_k_cities_m2

#******************** Model_M3 *************************#
def model_m3_proba(top_k_cities_m1, top_k_cities_m2, enriched_city = '../Model_M2/data/Cities_enriched.csv'):    
    
    df = pd.read_csv(enriched_city, sep='#')
    
    ## Enriching the Json output with cities info
    dict_city_country = dict()
    dict_city_flags = dict()
    dict_wiki = dict()
    dict_class = dict()
    dict_price = dict()
    dict_iata_code = dict()
    for el in df.values:
        dict_city_country[el[1]] = el[11]
    for el in df.values:
        dict_city_flags[el[1]] = 'assets/images/flags-mini/' + el[16].split('/')[1]
    for el in df.values:
        dict_wiki[el[1]] = el[17]
    for el in df.values:
        dict_class[el[1]] = 'card-image hero ' + el[1].lower()
    for el in df.values:
        dict_price[el[1]] = 'From $' + str(random.randint(150,300))
    for el in df.values:
        dict_iata_code[el[1]] = el[0]
    ## Computing the cross proba
    dict_m1 = dict()
    dict_m2 = dict()
    for i in range(len(top_k_cities_m1)):
        dict_m1[str(top_k_cities_m1[i]['City_name'])] = top_k_cities_m1[i]['Proba']
    for i in range(len(top_k_cities_m2)):
        dict_m2[str(top_k_cities_m2[i]['City_name'])] = top_k_cities_m2[i]['Proba']
    dict_mult = dict()
    for key in dict_m1:
        dict_mult[key] = dict_m1[key]*dict_m2[key]

    sorted_vec = sorted(dict_mult.items(), key=lambda kv: kv[1], reverse=True)
    sorted_dict_mult = collections.OrderedDict(sorted_vec)

    ## Returning the output Json
    output_list = list()
    for key in sorted_dict_mult:
        output_dict = dict()
        output_dict['city_name'] = key
        output_dict['iata_code'] = dict_iata_code[key]
        output_dict['proba'] = str(sorted_dict_mult[key])
        try:
            output_dict['country'] = dict_city_country[key]
            output_dict['flag'] = dict_city_flags[key]
            output_dict['superclass'] = 'col s12 m12 l3 xl3'
            output_dict['class'] = dict_class[key]
            output_dict['price'] = dict_price[key]
            output_dict['wiki'] = dict_wiki[key]
            output_dict['image'] = 'assets/images/cities_images/' + dict_iata_code[key] + '_0.jpg'
        except:
            output_dict['country'] = ''
            output_dict['flag'] = ''
            output_dict['superclass'] = ''
            output_dict['class'] = ''
            output_dict['price'] = ''
            output_dict['wiki'] = ''
            output_dict['image'] = ''
        #try:
        #    output_dict['image_url'] = spotpix_image(dict_city_country[key], dict_iata_code[key])
        #except:
        #    output_dict['image_url'] = ''    
        output_list.append(output_dict)
    return output_list

@app.route('/', methods=['GET', 'POST', 'OPTIONS'])
def root_func():

    if request.method == 'OPTIONS':
        return '{"status": 200}\n'
    else:     
        if len(request.data) != 0:
            payload = json.loads(request.data)

            dpt_day = payload['dpt_day']
            nip = payload['nip']
            stay_duration = payload['stay_duration']
            user_id = payload['user_id']
            top_k = payload['top_k']
            image_file_path = payload['image_file_path']

        elif len(request.form) !=0 :

            dpt_day = int(request.form.get("dpt_day"))
            nip = int(request.form.get("nip"))
            stay_duration = int(request.form.get("stay_duration"))
            user_id = int(request.form.get("user_id"))
            top_k = int(request.form.get("top_k"))
            image_file_path = request.form.get('image_file_path')
        else:
            dpt_day = int(request.args.get("dpt_day"))
            nip = int(request.args.get("nip"))
            stay_duration = int(request.args.get("stay_duration"))
            user_id = int(request.args.get("user_id"))
            top_k = int(request.args.get("top_k"))
            image_file_path = request.args.get('image_file_path')

        ## Dictionnary city2image
        path = '../Model_M2/data/'
        city2img = {'NY.jpg': path + 'New_York.jpg', 'SF.jpg': path + 'San_Francisco.jpg', 'GV.jpg': path + 'Gorges_Verdon.jpg', 'BA.jpg': path + 'Barcelona.jpg','CA.jpg': path + 'Casablanca-Morocco.jpg', 'DU.jpg': path + 'Kings_Landing.jpg', 'NZ.jpg': path + 'Cove_Beach_NZ.jpg', 'RM.jpg': path + 'Roma.jpg', 'SW.jpg': path + 'Switzerland.jpg'}

        if image_file_path in city2img:
            image_file_path = city2img[image_file_path]

        top_k_cities_m1 = model_m1_proba(dpt_day=dpt_day, nip=nip, stay_duration=stay_duration, user_id=user_id)
        start_time = time.time()
        top_k_cities_m2 = model_m2_proba(image_path=image_file_path)
        print('Model 2')
        print("--- %s seconds ---" % (time.time() - start_time))
        start_time = time.time()
        top_k_cities_m3 = model_m3_proba(top_k_cities_m1, top_k_cities_m2)
        print('Model 3')
        print("--- %s seconds ---" % (time.time() - start_time))
        TOP_K = top_k_cities_m3[0:top_k]
        enriched_TOP_K = []
        start_time = time.time()        
        for el in TOP_K:
            output_dict = el.copy()
            try:
                output_dict['image_url'] = spotpix_image(el['country'], el['iata_code'])
            except:
                output_dict['image_url'] = '' 
            enriched_TOP_K.append(output_dict)
        print('Enriching')
        print("--- %s seconds ---" % (time.time() - start_time))        
        print(enriched_TOP_K)
        return jsonify(enriched_TOP_K)