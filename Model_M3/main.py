import json
import pickle
import sys
import os
import tensorflow as tf
import numpy as np
import pandas as pd
import collections
import argparse
import random

sys.path.append('../Model_M2/src/')
sys.path.append('../Model_M2/utils/')
sys.path.append('../Model_M1/src/')
sys.path.append('../Model_M1/utils/')
import Model_M2
import Model_M1


#******************** Model_M1 *************************#
def model_m1_proba(predictive_model, dpt_day=3, nip=2, stay_duration=2, user_id=5):

    with open('../Model_M1/data/fin_user_item_interaction.pkl', 'rb') as f:
        user_item_interaction =  pickle.load(f)

    print('*************************** Loading cities names & characteristics: ****************************************')

    cities_index = np.load('../Model_M1/data/cities_name_index.npy')

    item_charac = np.load('../Model_M1/data/norm_item_matrix.npy')

    print('*************************** Loading user demographics: ****************************************')

    user_charac = np.load('../Model_M1/data/norm_user_matrix.npy')

    print('*************************** Instantiate DKFM Model : ****************************************')

    pwd = os.getcwd()
    os.chdir('../Model_M1/src/')
    model_m1 = Model_M1.Model_M1(user_item_interaction, cities_index, item_charac, user_charac, predictive_model)

    print('*************************** Compute Model input: ****************************************') 
    X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_ = model_m1.compute_input(dpt_day, nip, stay_duration, user_id)

    print('*************************** Compute the top-k most similar cities: ****************************************')
    top_k_cities_m1 = model_m1.compute_recommendation(X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_, k=119, user_id=user_id)
    
    os.chdir(pwd)
    
    return top_k_cities_m1

#******************** Model_M2 *************************#
def model_m2_proba(image_path):

    print('*************************** Loading the Model: ****************************************')

    with open('../Model_M2/data/fastext_cities.pkl', 'rb') as f:
        dict_fastext_cities_key_words = pickle.load(f)

    print('*************************** Loading the Word Vectors: ****************************************')
    with open('../Model_M2/data/dict_idf.pkl', 'rb') as f:
        dict_idf = pickle.load(f)
    with open('../Model_M2/data/fastext_dictionary.pkl', 'rb') as f:
        dict_fasttext = pickle.load(f)

    print('*************************** Instantiate the Model: ****************************************')
    model_m2 = Model_M2.Model_M2('../Model_M2/Google_Credentials/Cred.json', dict_fastext_cities_key_words, dict_idf, dict_fasttext)


    print('*************************** Compute the image embedding: ****************************************')
    fastext_vector = model_m2.compute_image_fastext(image_path)

    print('*************************** Compute the top-k most similar cities: ****************************************')
    top_k_cities_m2 = model_m2.compute_top_k_similarity(fastext_vector, 119)
    
    return top_k_cities_m2

#******************** Model_M3 *************************#
def model_m3_proba(top_k_cities_m1, top_k_cities_m2, enriched_city = '../Model_M2/data/Cities_enriched.csv'):    
    
    df = pd.read_csv(enriched_city, sep='#')
    
    ## Enriching the Json output with cities info
    dict_city_country = dict()
    dict_city_flags = dict()
    dict_wiki = dict()
    dict_class = dict()
    dict_price = dict()
    for el in df.values:
        dict_city_country[el[1]] = el[11]
    for el in df.values:
        dict_city_flags[el[1]] = 'assets/images/flags-mini/' + el[16].split('/')[1]
    for el in df.values:
        dict_wiki[el[1]] = el[17]
    for el in df.values:
        dict_class[el[1]] = 'card-image hero ' + el[1].lower()
    for el in df.values:
        dict_price[el[1]] = 'From $' + str(random.randint(150,300))

    ## Computing the cross proba
    dict_m1 = dict()
    dict_m2 = dict()
    for i in range(len(top_k_cities_m1)):
        dict_m1[str(top_k_cities_m1[i]['City_name'])] = top_k_cities_m1[i]['Proba']
    for i in range(len(top_k_cities_m2)):
        dict_m2[str(top_k_cities_m2[i]['City_name'])] = top_k_cities_m2[i]['Proba']
    dict_mult = dict()
    for key in dict_m1:
        dict_mult[key] = dict_m1[key]*dict_m2[key]

    sorted_vec = sorted(dict_mult.items(), key=lambda kv: kv[1], reverse=True)
    sorted_dict_mult = collections.OrderedDict(sorted_vec)

    ## Returning the output Json
    output_list = list()
    for key in sorted_dict_mult:
        output_dict = dict()
        output_dict['city_name'] = key
        output_dict['proba'] = sorted_dict_mult[key]
        try:
            output_dict['country'] = dict_city_country[key]
            output_dict['flag'] = dict_city_flags[key]
            output_dict['superclass'] = 'col s12 m12 l6 xl3'
            output_dict['class'] = dict_class[key]
            output_dict['price'] = dict_price[key]
            output_dict['wiki'] = dict_wiki[key]
        except:
            output_dict['country'] = ''
            output_dict['flag'] = ''
            output_dict['superclass'] = ''
            output_dict['class'] = ''
            output_dict['price'] = ''
            output_dict['wiki'] = ''
        el_folder_name = "_".join(key.split(" "))
        output_dict['image'] = '../Model_M2/data/images/' + el_folder_name + '_images/' + el_folder_name + '_images_1'
        output_list.append(output_dict)
        
    return output_list

def main():
    
    parser = argparse.ArgumentParser(description='M3')
    '''
    Arguments:
        file path: the file of the image to compute the similarity cities with 
        top_k: top-k cities you want to display
    '''
    parser.add_argument('--image_file_path', type=str, default='../Model_M2/data/Kings_Landing.jpg')
    parser.add_argument('--model', type=str, default='../models/DKFM_10.ckpt')
    parser.add_argument('--dpt_day', type=int, default=0)
    parser.add_argument('--nip', type=int, default=1)
    parser.add_argument('--stay_duration', type=int, default=2)
    parser.add_argument('--user_id', type=int, default=0)
    parser.add_argument('--top_k', type=int, default=5)
    args = parser.parse_args()
    print(args)
    
    top_k_cities_m1 = model_m1_proba(predictive_model=args.model, dpt_day=args.dpt_day, nip=args.nip, stay_duration=args.stay_duration, user_id=args.user_id)
    top_k_cities_m2 = model_m2_proba(image_path=args.image_file_path)
    top_k_cities_m3 = model_m3_proba(top_k_cities_m1, top_k_cities_m2)
    
    print(top_k_cities_m3[0:args.top_k])

if __name__ == '__main__':
    main()