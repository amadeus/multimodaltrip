from flask import Flask, url_for, request, make_response, jsonify, Response
import json
import pickle
import numpy as np

import Model_M1
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

with open('../data/fin_user_item_interaction.pkl', 'rb') as f:
    user_item_interaction = pickle.load(f)

print('*************************** Loading cities names & characteristics: ****************************************')
cities_index = np.load('../data/cities_name_index.npy')
item_charac = np.load('../data/norm_item_matrix.npy')

print('*************************** Loading user demographics: ****************************************')
user_charac = np.load('../data/norm_user_matrix.npy')

print('*************************** Instantiate DKFM Model : ****************************************')
models = Model_M1.Model_M1(user_item_interaction, cities_index, item_charac, user_charac, '../models/DKFM_10.ckpt')

@app.route('/', methods=['GET', 'POST', 'OPTIONS'])
def root_func():    

    if request.method == 'OPTIONS':
        return '{"status": 200}\n'
    else: 
        if len(request.data) != 0:
            payload = json.loads(request.data)

            dpt_day = payload['dpt_day']
            nip = payload['nip']
            stay_duration = payload['stay_duration']
            user_id = payload['user_id']
            top_k = payload['top_k']

        elif len(request.form) !=0 :

            dpt_day = int(request.form.get("dpt_day"))
            nip = int(request.form.get("nip"))
            stay_duration = int(request.form.get("stay_duration"))
            user_id = int(request.form.get("user_id"))
            top_k = int(request.form.get("top_k"))
        else:
            dpt_day = int(request.args.get("dpt_day"))
            nip = int(request.args.get("nip"))
            stay_duration = int(request.args.get("stay_duration"))
            user_id = int(request.args.get("user_id"))
            top_k = int(request.args.get("top_k"))

        print('*************************** Compute the image embedding: ****************************************')
        X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_ = models.compute_input(dpt_day=dpt_day, nip=nip, stay_duration=stay_duration, user_id=user_id)

        print('*************************** Compute the top-k most similar cities: ****************************************')
        top_k_cities = models.compute_recommendation(X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_, k=top_k, user_id=user_id)

        print('*************************** Enrich the output: ****************************************')
        enriched_top_k_cities = models.enrich_output(top_k_cities, enriched_city = '../../Model_M2/data/Cities_enriched.csv')

        print(enriched_top_k_cities)

        return jsonify(enriched_top_k_cities)