import numpy as np
import tensorflow as tf
import numpy as np
import sys
sys.path.append('../utils/')
from Utils import *
import Model_M1
import argparse
import pickle


def main():
    parser = argparse.ArgumentParser(description='M1')
    '''
    Arguments:
        file path: the file of the image to compute the similarity cities with 
        top_k: top-k cities you want to display
    '''
    parser.add_argument('--model', type=str, default='../models/DKFM_10.ckpt')
    parser.add_argument('--dpt_day', type=int, default=0)
    parser.add_argument('--nip', type=int, default=1)
    parser.add_argument('--stay_duration', type=int, default=2)
    parser.add_argument('--user_id', type=int, default=0)
    parser.add_argument('--top_k', type=int, default=5)
    args = parser.parse_args()
    print(args)
    
    
    
    with open('../data/fin_user_item_interaction.pkl', 'rb') as f:
        user_item_interaction =  pickle.load(f)
    
    print('*************************** Loading cities names & characteristics: ****************************************')

    cities_index = np.load('../data/cities_name_index.npy')
    
    item_charac = np.load('../data/norm_item_matrix.npy')
    
    print('*************************** Loading user demographics: ****************************************')

    user_charac = np.load('../data/norm_user_matrix.npy')
    
    print('*************************** Instantiate DKFM Model : ****************************************')
    
    models = Model_M1.Model_M1(user_item_interaction, cities_index, item_charac, user_charac, args.model)
   
    print('*************************** Compute the image embedding: ****************************************') 
    X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_ = models.compute_input(dpt_day=args.dpt_day, nip=args.nip, stay_duration=args.stay_duration, user_id=args.user_id)

    print('*************************** Compute the top-k most similar cities: ****************************************')
    top_k_cities = models.compute_recommendation(X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_, k=args.top_k, user_id=args.user_id)
    
    print('*************************** Enrich the output: ****************************************')
    enriched_top_k_cities = models.enrich_output(top_k_cities, enriched_city = '../../Model_M2/data/Cities_enriched.csv')
    
    print(enriched_top_k_cities)
    
if __name__ == '__main__':
    main()
