import numpy as np
import tensorflow as tf
import numpy as np
from scipy.special import softmax
import os
import sys
import pandas as pd
import random
sys.path.append('../utils/')
from Utils import *
import json

class Model_M1:
    
    def __init__(self, user_item_interaction, cities_index, item_charac, user_charac, model):
        self.user_item_interaction = user_item_interaction
        ## cities matrix index
        self.cities_index = cities_index
        ## cities characteristics (w2vec + kge)
        self.item_charac = item_charac
        ## users demographics
        self.user_charac = user_charac 
        ## pretrained DKFM model
        self.model = model
        
        
    def compute_input(self, dpt_day=0, nip=2, stay_duration=2, user_id=0):
        # CTXT vector
        dpt_day_vec = np.zeros(7)
        dpt_day_vec[dpt_day] = 1
        dpt_day_vec = np.reshape(dpt_day_vec, (1,-1))
        nip_vec = np.zeros(10)
        nip_vec[nip] = 1
        nip_vec = np.reshape(nip_vec, (1,-1))
        stay_duration_scal = np.array([stay_duration]).reshape(1,-1)
        X_ctxt = np.tile(np.concatenate([dpt_day_vec, nip_vec, stay_duration_scal], axis=1), (119,1))
        # user_item_vector
        user_vec = self.user_charac[user_id].reshape(1,-1)
        user_item_features = np.concatenate([np.tile(user_vec, (119,1)), self.item_charac], axis=1)
        # x_user
        X_user = np.tile(user_id, (119,))
        # x_item
        X_item = np.arange(0,119,1).reshape(-1,)
        #Y_labels (however)
        Y_labels = np.zeros((119,1))
        return X_user, X_item, user_item_features, X_ctxt, Y_labels
    
    def compute_recommendation(self, X_user_, X_item_, user_item_features_, X_ctxt_, Y_labels_, k=10, user_id=0):
    

        os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"]="0"
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
    
        with tf.Session(config = config) as session:
            saver = tf.train.import_meta_graph('../models/DKFM_10.ckpt.meta')
            saver.restore(session, self.model)                
            
            # Now, let's access and create placeholders variables and
            # create feed-dict to feed new data

            graph = tf.get_default_graph()
            X_user = graph.get_tensor_by_name("X_user:0")
            X_item = graph.get_tensor_by_name("X_item:0")
            X_ctxt = graph.get_tensor_by_name("X_ctx:0")
            user_item_features = graph.get_tensor_by_name("user_item_features:0")
            Y_labels = graph.get_tensor_by_name("Y_labels:0")
            logits = graph.get_tensor_by_name("logits:0")
            loss_op = graph.get_tensor_by_name("loss_op:0")
            
            logits_, test_loss_op= session.run([logits, loss_op], feed_dict={X_user:X_user_, X_item:X_item_, 
                                        user_item_features:user_item_features_,
                                        X_ctxt:X_ctxt_, 
                                        Y_labels: Y_labels_})

            Pred = list()
            for el in logits_:
                Pred.append(sigmoid(el)[0])
        Pred = np.array(Pred)
        Ordered_cities = list(np.argsort(Pred)[::-1])
        for el in self.user_item_interaction[user_id]:
            Ordered_cities.remove(el)
        Ordered_cities_name = self.cities_index[list(Ordered_cities)]
        Ordered_cities_proba = Pred[list(Ordered_cities)]
        K_Ordered_cities_name = Ordered_cities_name[0:k]
        K_Ordered_cities_proba = softmax(Ordered_cities_proba[0:k])
        returned_top_k_list = list()
        for i, el in enumerate(K_Ordered_cities_name):
            dict_city = dict()
            dict_city["City_name"] = el
            dict_city["Proba"] = K_Ordered_cities_proba[i]
            returned_top_k_list.append(dict_city)
        return returned_top_k_list
    
    def enrich_output(self, returned_top_k_list, enriched_city = '../../Model_M2/data/Cities_enriched.csv'):
        
        df = pd.read_csv(enriched_city, sep='#')
    
        ## Enriching the Json output with cities info
        dict_city_country = dict()
        dict_city_flags = dict()
        dict_wiki = dict()
        dict_class = dict()
        dict_price = dict()
        dict_iata_code = dict()
        for el in df.values:
            dict_city_country[el[1]] = el[11]
        for el in df.values:
            dict_city_flags[el[1]] = 'assets/images/flags-mini/' + el[16].split('/')[1]
        for el in df.values:
            dict_wiki[el[1]] = el[17]
        for el in df.values:
            dict_class[el[1]] = 'card-image hero ' + el[1].lower()
        for el in df.values:
            dict_price[el[1]] = 'From $' + str(random.randint(150,300))
        for el in df.values:
            dict_iata_code[el[1]] = el[0]
        output_list = list()
        for city in returned_top_k_list:
            output_dict = dict()
            key = city['City_name']
            output_dict['city_name'] = key
            output_dict['proba'] = str(city['Proba'])
            try:
                output_dict['country'] = dict_city_country[key]
                output_dict['flag'] = dict_city_flags[key]
                output_dict['superclass'] = 'col s12 m12 l3 xl3'
                output_dict['class'] = dict_class[key]
                output_dict['price'] = dict_price[key]
                output_dict['wiki'] = dict_wiki[key]
                output_dict['image'] = 'assets/images/cities_images/' + dict_iata_code[key] + '_0.jpg'
            except:
                output_dict['country'] = ''
                output_dict['flag'] = ''
                output_dict['superclass'] = ''
                output_dict['class'] = ''
                output_dict['price'] = ''
                output_dict['wiki'] = ''
                output_dict['image'] = ''
            try:
                output_dict['image_url'] = spotpix_image(dict_city_country[key], dict_iata_code[key])
            except:
                output_dict['image_url'] = ''
            output_list.append(output_dict)
        return output_list