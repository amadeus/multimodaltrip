import io
import os
import numpy as np
import pandas as pd
import random
import pickle
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types
from google.oauth2 import service_account
from IPython.display import Image
from sklearn.metrics.pairwise import cosine_similarity
from numpy.linalg import norm
import operator
import sys
sys.path.append('../utils')
from utils_func import *
from scipy.special import softmax

class Model_M2:
    
    def __init__(self, google_credientials_file, dict_fastext_cities_key_words, dict_idf, dict_fasttext):
        ## Google credentials
        self.credentials = service_account.Credentials.from_service_account_file(google_credientials_file)
        ## fastext cities dictionary
        self.dict_fastext_cities_key_words = dict_fastext_cities_key_words
        ## word vectors
        self.dict_idf = dict_idf
        self.dict_fasttext = dict_fasttext

    def compute_keywords_fastext(self, corpus):
        ## Computing the keywordss fasttext vector
        corpus = corpus.replace(" ", "")
        item = corpus.split(',')
        w_array = np.zeros(300)
        k_words = 0
        for k in range(len(item)):
            if item[k].lower() in self.dict_fasttext:
                w = self.dict_fasttext[item[k].lower()]
                if len(w) > 0 and item[k].lower() in self.dict_idf :
                    k_words += 1 
                    w = self.dict_idf[item[k].lower()]*w
                    w_array += w
        w_array = w_array/k_words
        fastext_vector  = w_array.reshape((1,-1))

        return fastext_vector
        
        
    def compute_image_fastext(self, path):

        # Computing the 
        list_labels = list()
        list_web_entities = list()
        city_content = list()
        
        # Instantiate Google client
        client = vision.ImageAnnotatorClient(credentials=self.credentials)
        
        if path.startswith('http') or path.startswith('gs:'):
            image = types.Image()
            image.source.image_uri = path

        else:
            with io.open(path, 'rb') as image_file:
                content = image_file.read()

            image = types.Image(content=content)

        # Performs label detection on the image file
        Labels = client.label_detection(image=image).label_annotations
        Web_annotations = client.web_detection(image=image).web_detection.web_entities
        
        ##
        for label in Labels:
            des = label.description
            if len(des) > 0:
                list_labels.append(des)
        dict_web_entities = dict()
        for web_annot in Web_annotations:
            des = web_annot.description
            if len(des) > 0:
                list_web_entities.append(des)
        for x in list_labels:
            city_content.append(x)
        for x in list_web_entities:
            city_content.append(x)
        corpus = " ".join(city_content)

        ## Computing the image fasttext vector
        item = corpus.split()
        w_array = np.zeros(300)
        k_words = 0
        for k in range(len(item)):
            if item[k] in self.dict_fasttext:
                w = self.dict_fasttext[item[k]]
                if len(w) > 0 and item[k] in self.dict_idf :
                    k_words += 1 
                    w = self.dict_idf[item[k]]*w
                    w_array += w
        w_array = w_array/k_words
        fastext_vector  = w_array.reshape((1,-1))

        return fastext_vector
    
    def compute_top_k_similarity(self, fastext_vector, p = 5):
        
        dict_sim = dict()
        for key in self.dict_fastext_cities_key_words:
            a = fastext_vector
            b = self.dict_fastext_cities_key_words[key].reshape((1,-1))
            dict_sim[key] = cosine_similarity(a, b, None)
        sorted_dict_sim = sorted(dict_sim.items(), key=operator.itemgetter(1), reverse=True)
        k = 0
        destination_vec = list()
        distance_vec = list()
        for i in sorted_dict_sim:
            # The name of the image file to annotate
            k += 1
            destination_vec.append(i[0])
            distance_vec.append(i[1][0][0])#distance_vec.append(sigmoid(-i[1]))
            if k>p:
                break
        proba_vec = distance_vec#softmax(np.array(distance_vec))
        returned_top_k_list = list()
        for i, el in enumerate(destination_vec):
            dict_city = dict()
            dict_city['City_name'] = el
            dict_city['Proba'] = proba_vec[i]
            returned_top_k_list.append(dict_city)   
        return returned_top_k_list
    
    def enrich_output(self, returned_top_k_list, enriched_city = '../data/Cities_enriched.csv'):
        
        df = pd.read_csv(enriched_city, sep='#')
    
        ## Enriching the Json output with cities info
        dict_city_country = dict()
        dict_city_flags = dict()
        dict_wiki = dict()
        dict_class = dict()
        dict_price = dict()
        dict_iata_code = dict()
        for el in df.values:
            dict_city_country[el[1]] = el[11]
        for el in df.values:
            dict_city_flags[el[1]] = 'assets/images/flags-mini/' + el[16].split('/')[1]
        for el in df.values:
            dict_wiki[el[1]] = el[17]
        for el in df.values:
            dict_class[el[1]] = 'card-image hero ' + el[1].lower()
        for el in df.values:
            dict_price[el[1]] = 'From $' + str(random.randint(150,300))
        for el in df.values:
            dict_iata_code[el[1]] = el[0]
        output_list = list()            
        for city in returned_top_k_list:
            output_dict = dict()
            key = city['City_name']
            output_dict['city_name'] = key
            output_dict['proba'] = str(city['Proba'])
            try:
                output_dict['country'] = dict_city_country[key]
                output_dict['flag'] = dict_city_flags[key]
                output_dict['superclass'] = 'col s12 m12 l3 xl3'
                output_dict['class'] = dict_class[key]
                output_dict['price'] = dict_price[key]
                output_dict['wiki'] = dict_wiki[key]
                output_dict['image'] = 'assets/images/cities_images/' + dict_iata_code[key] + '_0.jpg'
            except:
                output_dict['country'] = ''
                output_dict['flag'] = ''
                output_dict['superclass'] = ''
                output_dict['class'] = ''
                output_dict['price'] = ''
                output_dict['wiki'] = ''
                output_dict['image'] = ''
            try:
                output_dict['image_url'] = spotpix_image(dict_city_country[key], dict_iata_code[key])
            except:
                output_dict['image_url'] = ''
            output_list.append(output_dict)
        return output_list