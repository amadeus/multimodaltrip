from flask import Flask, url_for, request
import json
import pickle

import Model_M2

app = Flask(__name__)

print('*************************** Loading the Model: ****************************************')

with open('../data/fastext_cities.pkl', 'rb') as f:
    dict_fastext_cities_key_words = pickle.load(f)

print('*************************** Loading the Word Vectors: ****************************************')
with open('../data/dict_idf.pkl', 'rb') as f:
    dict_idf = pickle.load(f)
with open('../data/fastext_dictionary.pkl', 'rb') as f:
    dict_fasttext = pickle.load(f)

print('*************************** Instantiate the Model: ****************************************')
models = Model_M2.Model_M2('../Google_Credentials/Cred.json', dict_fastext_cities_key_words, dict_idf, dict_fasttext)


@app.route('/')
def root_func():
    payload = json.loads(request.data)
    
    print('*************************** Compute the image embedding: ****************************************')
    fastext_vector = models.compute_keywords_fastext(payload['keywords'])

    print('*************************** Compute the top-k most similar cities: ****************************************')
    top_k_cities = models.compute_top_k_similarity(fastext_vector, p = payload['top_k'] )

    print('*************************** Enrich the output: ****************************************')
    enriched_top_k_cities = models.enrich_output(top_k_cities, enriched_city = '../data/Cities_enriched.csv')
    
    print(enriched_top_k_cities)
    
    return str(enriched_top_k_cities)