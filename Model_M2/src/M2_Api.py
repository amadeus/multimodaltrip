from flask import Flask, url_for, request, jsonify
import json
import pickle

import Model_M2
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

print('*************************** Loading the Model: ****************************************')

with open('../data/fastext_cities.pkl', 'rb') as f:
    dict_fastext_cities_key_words = pickle.load(f)

print('*************************** Loading the Word Vectors: ****************************************')
with open('../data/dict_idf.pkl', 'rb') as f:
    dict_idf = pickle.load(f)
with open('../data/fastext_dictionary.pkl', 'rb') as f:
    dict_fasttext = pickle.load(f)

print('*************************** Instantiate the Model: ****************************************')
models = Model_M2.Model_M2('../Google_Credentials/Cred.json', dict_fastext_cities_key_words, dict_idf, dict_fasttext)

@app.route('/', methods=['GET', 'POST', 'OPTIONS'])
def root_func():
   
    if request.method == 'OPTIONS':
        return '{"status": 200}\n'
    else:     
        if len(request.data) != 0:
            payload = json.loads(request.data)
            image_file_path = payload['image_file_path']
            top_k = payload['top_k']
        elif len(request.form) !=0 :
            image_file_path = request.form.get("image_file_path")
            top_k = int(request.form.get("top_k"))
        else:
            image_file_path = request.args.get("image_file_path")
            top_k = int(request.args.get("top_k"))


        ## Dictionnary city2image
        path = '../data/'
        city2img = {'NY.jpg': path + 'New_York.jpg', 'SF.jpg': path + 'San_Francisco.jpg', 'GV.jpg': path + 'Gorges_Verdon.jpg', 'BA.jpg': path + 'Barcelona.jpg','CA.jpg': path + 'Casablanca-Morocco.jpg', 'DU.jpg': path + 'Kings_Landing.jpg', 'NZ.jpg': path + 'Cove_Beach_NZ.jpg', 'RM.jpg': path + 'Roma.jpg', 'SW.jpg': path + 'Switzerland.jpg'}

        if image_file_path in city2img:
            image_file_path = city2img[image_file_path]

        print('*************************** Compute the image embedding: ****************************************')
        fastext_vector = models.compute_image_fastext(image_file_path)

        print('*************************** Compute the top-k most similar cities: ****************************************')
        top_k_cities = models.compute_top_k_similarity(fastext_vector, p = top_k)

        print('*************************** Enrich the output: ****************************************')
        enriched_top_k_cities = models.enrich_output(top_k_cities, enriched_city = '../data/Cities_enriched.csv')

        print(enriched_top_k_cities)

        return jsonify(enriched_top_k_cities)