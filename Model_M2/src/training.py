import io
import os
import pandas as pd
import numpy as np
import json
import pickle
import random
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types
from google.oauth2 import service_account
from IPython.display import Image
import io
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize
from sklearn.metrics.pairwise import cosine_similarity
from numpy.linalg import norm

credentials = service_account.Credentials.from_service_account_file('../Google_Credentials/My First Project-422c671b92f1.json')
# Instantiates a client
client = vision.ImageAnnotatorClient(credentials=credentials)

#Retrieve all the cities
list_cities_dir = list()
path = 'path_to/google-images-download/google_images_download/downloads'
files = os.listdir(path)
for name in files:
    if name != '.ipynb_checkpoints':
        list_cities_dir.append(name)
        
list_im_cities_dir = list()
path = 'path_to/google_images_download/downloads/' + list_cities_dir[1]
files = os.listdir(path)
for name in files:
    list_im_cities_dir.append(name)
    
dict_cities = dict()
for i, city in enumerate(list_cities_dir):
    list_im_cities_dir = list()
    path = 'path_to/google_images_download/downloads/' + city
    city_name_l = city.split("_")
    city_name = " ".join(city_name_l[0:len(city_name_l)-1])
    print(city_name)
    files = os.listdir(path)
    for name in files:
        list_im_cities_dir.append(name)
    dict_city = dict()
    list_labels = list()
    list_web_entities = list()
    for city_im in list_im_cities_dir:
        # The name of the image file to annotate
        file_name = path + '/' + city_im
        try:
            Image(file_name, width=400, height=400)
            # Loads the image into memory
            with io.open(file_name, 'rb') as image_file:
                content = image_file.read()

            image = types.Image(content=content)

            # Performs label detection on the image file
            Labels = client.label_detection(image=image).label_annotations
            Web_annotations = client.web_detection(image=image).web_detection.web_entities
            ##
            for label in Labels:
                des = label.description
                if len(des) > 0:
                    list_labels.append(des)
            dict_web_entities = dict()
            for web_annot in Web_annotations:
                des = web_annot.description
                if len(des) > 0:
                    list_web_entities.append(des)
        except:
            print("An exception occurred") 
    dict_city['Labels'] = list_labels
    dict_city['Web_Entities'] = list_web_entities
    dict_cities[city_name] = dict_city
    
    
with open('Cities_Key_Word.json', 'w') as fp:
    json.dump(dict_cities, fp, sort_keys=True, indent=4)

corpus = list()
for key in dict_json:
    city_content = list()
    labels_list = dict_json[key]['Labels']
    web_entities_list = dict_json[key]['Web_Entities']
    for x in labels_list:
        city_content.append(x)
    for x in web_entities_list:
        city_content.append(x)
    corpus.append(" ".join(city_content))
    
with open('../data/fastext_dictionary.pkl', 'rb') as f:
    dict_fasttext = pickle.load(f)

vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(corpus)
dict_idf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))

with open('../data/dict_idf.pkl', 'wb') as f:
    pickle.dump(dict_idf, f, pickle.HIGHEST_PROTOCOL)
city2vec = list()
for i, item in enumerate(corpus):
    item = item.split()
    w_array = np.zeros(300)
    k_words = 0
    for k in range(len(item)):
        if item[k] in dict_fasttext:
            w = dict_fasttext[item[k]]
            if len(w) > 0 and item[k] in dict_idf :
                k_words += 1 
                w = dict_idf[item[k]]*w
                w_array += w
    w_array = w_array/k_words
    city2vec.append(w_array)
fasttext_cities = np.array(city2vec)
dict_fastext_cities_key_words = dict()
k = 0
for key in dict_json:
    dict_fastext_cities_key_words[key] = fasttext_cities[k,:]
    k += 1
    
with open('../data/fastext_cities.pkl', 'wb') as f:
    pickle.dump(dict_fastext_cities_key_words, f, pickle.HIGHEST_PROTOCOL)