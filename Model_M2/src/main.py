import io
import os
import numpy as np
import json
import pickle
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types
from google.oauth2 import service_account
#from IPython.display import Image
from sklearn.metrics.pairwise import cosine_similarity
from numpy.linalg import norm
import operator
import Model_M2
import argparse

def main():
    parser = argparse.ArgumentParser(description='M2')
    '''
    Arguments:
        file path: the file of the image to compute the similarity cities with 
        top_k: top-k cities you want to display
    '''
    parser.add_argument('--google_file_cred_path', type=str, default='../Google_Credentials/Cred.json')
    parser.add_argument('--image_file_path', type=str, default='../data/Kings_Landing.jpg')
    parser.add_argument('--top_k', type=int, default=5)
    args = parser.parse_args()
    print(args)
    
    print('*************************** Loading the Model: ****************************************')

    with open('../data/fastext_cities.pkl', 'rb') as f:
        dict_fastext_cities_key_words = pickle.load(f)
    
    print('*************************** Loading the Word Vectors: ****************************************')
    with open('../data/dict_idf.pkl', 'rb') as f:
        dict_idf = pickle.load(f)
    with open('../data/fastext_dictionary.pkl', 'rb') as f:
        dict_fasttext = pickle.load(f)
    
    print('*************************** Instantiate the Model: ****************************************')
    models = Model_M2.Model_M2(args.google_file_cred_path, dict_fastext_cities_key_words, dict_idf, dict_fasttext)
   
    print('*************************** Compute the image embedding: ****************************************') 
    fastext_vector = models.compute_image_fastext(args.image_file_path)

    print('*************************** Compute the top-k most similar cities: ****************************************')
    top_k_cities = models.compute_top_k_similarity(fastext_vector, p = args.top_k)

    print('*************************** Enrich the output: ****************************************')
    enriched_top_k_cities = models.enrich_output(top_k_cities, enriched_city = '../data/Cities_enriched.csv')
    
    print(enriched_top_k_cities)
    
if __name__ == '__main__':
    main()
