import io
import os
import pandas as pd
import numpy as np
import json
import pickle
import random
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types
from google.oauth2 import service_account
from IPython.display import Image
import io
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize
from sklearn.metrics.pairwise import cosine_similarity
from numpy.linalg import norm
import urllib.request
import zipfile

def load_vectors(fname):
    fin = io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = map(float, tokens[1:])
    return data

url = "https://dl.fbaipublicfiles.com/fasttext/vectors-english/crawl-300d-2M.vec.zip"
urllib.request.urlretrieve(url, "crawl-300d-2M.vec.zip")

# Create a ZipFile Object and load sample.zip in it
with zipfile.ZipFile('crawl-300d-2M.vec.zip', 'r') as zipObj:
    # Extract all the contents of zip file in current directory
    zipObj.extractall()
    fasttext = load_vectors("crawl-300d-2M.vec")

os.remove("crawl-300d-2M.vec")
os.remove("crawl-300d-2M.vec.zip")

dict_fasttext = dict()
for key in fasttext:
    dict_fasttext[key] = np.array(list(fasttext[key]))
with open('Model_M2/data/fastext_dictionary.pkl', 'wb') as f:
    pickle.dump(dict_fasttext, f, pickle.HIGHEST_PROTOCOL)