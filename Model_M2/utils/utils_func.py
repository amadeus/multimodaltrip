import numpy as np
import requests
import pandas as pd

def sigmoid(x):
    return (1/(1+np.exp(-x)))

def spotpix_image(country, city):
    
    # defining a params dict for the parameters to be sent to the API 
    PARAMS = {'country':country, 'city':city, 'format': "Custom 1"} 

    # sending get request and saving the response as response object 
    r = requests.get(url = "https://test-spotpix.1a-lab.net/v2/media/files/by-city",
                     params = PARAMS, headers={'x-api-key': '1JlltDlLB37dT227Kp8hj10lPOmDwaAegITvlm0i'}) 

    # extracting data in json format 
    image_url = r.json()['data']['attachmentUri']
    
    return image_url